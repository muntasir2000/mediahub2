package kaaktaruaa.com.mediahub2.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.RadioGroup;

import kaaktaruaa.com.mediahub2.R;
import kaaktaruaa.com.mediahub2.fragment.MoviesGenreFragment;
import kaaktaruaa.com.mediahub2.fragment.MoviesOverviewFragment;
import kaaktaruaa.com.mediahub2.util.Constant;

/**
 * Created by muntasir on 6/7/15.
 */
public class MoviesDashboardActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.movies_dashboard_layout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_movies_dashboard);
        setSupportActionBar(toolbar);

        if(getSupportActionBar()!=null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setupInitialFragment();
        setupRadioButtons();

    }

    private void setupInitialFragment(){
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.movies_dashboard_fragment_container, new MoviesOverviewFragment());
        transaction.commit();
    }

    private void setupRadioButtons(){
        RadioGroup mRadioGroup = (RadioGroup) findViewById(R.id.movies_dashboard_radio_group);
        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int id) {

                FragmentManager fragmentManager = MoviesDashboardActivity.this.getSupportFragmentManager();
                MoviesOverviewFragment moviesOverviewFragment;
                switch (id) {
                    case R.id.movies_dashboard_radio_overview:
                        moviesOverviewFragment = new MoviesOverviewFragment();
                        FragmentTransaction moviesFragmentTransaction = fragmentManager.beginTransaction();
                        moviesFragmentTransaction.replace(R.id.movies_dashboard_fragment_container, moviesOverviewFragment);
                        moviesFragmentTransaction.commit();
                        break;
                    case R.id.movies_dashboard_radio_genre:
                        Log.d(Constant.DEBUG_TAG, "Genre clicked");
                        MoviesGenreFragment moviesGenreFragment = new MoviesGenreFragment();
                        FragmentTransaction genreFragmentTransaction = fragmentManager.beginTransaction();
                        genreFragmentTransaction.replace(R.id.movies_dashboard_fragment_container, moviesGenreFragment);
                        genreFragmentTransaction.commit();
                        break;
                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


}
