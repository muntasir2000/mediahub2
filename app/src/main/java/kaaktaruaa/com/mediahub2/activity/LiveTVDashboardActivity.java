package kaaktaruaa.com.mediahub2.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import kaaktaruaa.com.mediahub2.R;
import kaaktaruaa.com.mediahub2.adapter.SliderRecyclerAdapter;
import kaaktaruaa.com.mediahub2.datasource.LiveTVDataSource;
import kaaktaruaa.com.mediahub2.datasource.bean.SliderItem;
import kaaktaruaa.com.mediahub2.datasource.listener.OnSlideItemsAvailableListener;
import kaaktaruaa.com.mediahub2.util.Tags;

/**
 * Created by muntasir on 6/18/15.
 */
public class LiveTVDashboardActivity extends AppCompatActivity implements OnSlideItemsAvailableListener{

    ProgressBar progressBar;
    RelativeLayout containerLayout;
    Context context;

    private List<SliderItem> sliderItemList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.livetv_dashboard_layout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.livetv_dashboard_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        showProgressbarAndHideContents();
        LiveTVDataSource.getInstance().requestAllSliderItems(this);

    }

    private void showProgressbarAndHideContents(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.livetv_dashboard_toolbar);
        setSupportActionBar(toolbar);

        progressBar = (ProgressBar) findViewById(R.id.livetv_dashboard_progressbar);
        containerLayout = (RelativeLayout) findViewById(R.id.livetv_dashboard_container);

        progressBar.setVisibility(View.VISIBLE);
        containerLayout.setVisibility(View.GONE);
    }

    private void setupRecyclerViews(){
        RecyclerView generalRecyclerView = (RecyclerView) findViewById(R.id.livetv_dashboard_general_recyclerview);
        RecyclerView entertainmentRecyclerView = (RecyclerView) findViewById(R.id.livetv_dashboard_entertainment_recyclerview);
        RecyclerView newsRecyclerView = (RecyclerView) findViewById(R.id.livetv_dashboard_news_recyclerview);

        LinearLayoutManager generalRVlayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager entertainmentRVLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager newsRVLayoutManger = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);

        generalRecyclerView.setLayoutManager(generalRVlayoutManager);
        entertainmentRecyclerView.setLayoutManager(entertainmentRVLayoutManager);
        newsRecyclerView.setLayoutManager(newsRVLayoutManger);

        generalRecyclerView.setAdapter(new SliderRecyclerAdapter(this, getGeneralTaggedSliderItems()));
        entertainmentRecyclerView.setAdapter(new SliderRecyclerAdapter(this, getEntertainmentTaggedSliderItems()));
        newsRecyclerView.setAdapter(new SliderRecyclerAdapter(this, getNewsTaggedSliderItems()));

    }

    private List<SliderItem> getGeneralTaggedSliderItems(){
        ArrayList<SliderItem> found = new ArrayList<>();
        for(SliderItem item : sliderItemList){
            if(item.getTagsList().contains(Tags.GENERAL)){
                found.add(item);
            }
        }
        return found;
    }

    private List<SliderItem> getEntertainmentTaggedSliderItems(){
        ArrayList<SliderItem> found = new ArrayList<>();
        for(SliderItem item : sliderItemList){
            if(item.getTagsList().contains(Tags.ENTERTAINMENT)){
                found.add(item);
            }
        }
        return found;
    }

    private List<SliderItem> getNewsTaggedSliderItems(){
        ArrayList<SliderItem> found = new ArrayList<>();
        for(SliderItem item : sliderItemList){
            if(item.getTagsList().contains(Tags.NEWS)){
                found.add(item);
            }
        }
        return found;
    }
    @Override
    public void onSlideItemsDataAvailable(List<SliderItem> sliderItems) {
        this.sliderItemList = sliderItems;
        setupRecyclerViews();
        showContentAndHideProgressbar();
    }

    private void showContentAndHideProgressbar(){
        progressBar.setVisibility(View.GONE);
        containerLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
