package kaaktaruaa.com.mediahub2.activity;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;


import kaaktaruaa.com.mediahub2.R;
import kaaktaruaa.com.mediahub2.datasource.MoviesDataSource;
import kaaktaruaa.com.mediahub2.datasource.bean.Movie;
import kaaktaruaa.com.mediahub2.datasource.listener.OnMovieDetailsAvailableListener;

/**
 * Created by muntasir on 6/30/15.
 */
public class PlayMovieActivity extends AppCompatActivity implements OnMovieDetailsAvailableListener {

    String id;
    ProgressBar progressBar;
    RelativeLayout contentLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.play_movie_activity_layout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.play_movie_toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

   //     LibsChecker.checkVitamioLibs(this);
        Bundle incomingBundle = getIntent().getExtras();
        if (!incomingBundle.containsKey("id")) {
            throw new RuntimeException("String extra 'id' not passed. Bundle doesn't contain string 'id'. Pass a string movie id");
        }
        id = incomingBundle.getString("id");
        showProgressBar();

        MoviesDataSource.getInstance().requestMovieDetails(this, id);


    }

    private void showProgressBar() {
        progressBar = (ProgressBar) findViewById(R.id.play_movie_progressbar);
        contentLayout = (RelativeLayout) findViewById(R.id.play_movie_content_relativelayout);
        progressBar.setVisibility(View.VISIBLE);
        contentLayout.setVisibility(View.GONE);
    }

    private void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
        contentLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onDetailsLoaded(Movie movie) {
        hideProgressBar();

        final VideoView videoView = (VideoView) findViewById(R.id.play_movie_videoview);
        TextView titleTextView = (TextView) findViewById(R.id.play_movie_title_textview);
        TextView descriptionTextView = (TextView) findViewById(R.id.play_movie_description_textview);
        RatingBar ratingBar = (RatingBar) findViewById(R.id.play_movie_ratingbar);
        TextView starringTextView = (TextView) findViewById(R.id.play_movie_starring_textview);

        videoView.setVideoURI(Uri.parse(movie.getStreamUrl()));
        titleTextView.setText(movie.getTitle());
        descriptionTextView.setText(movie.getDescription());
        ratingBar.setRating(movie.getRating());
        starringTextView.setText(movie.getStarringActorNamesListJson());

        videoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoView.start();
                Toast.makeText(v.getContext(), "Video should start", Toast.LENGTH_SHORT).show();
            }
        });

        MediaController mediaController = new MediaController(this, true);
        videoView.setMediaController(mediaController);

        videoView.start();

    }
}
