package kaaktaruaa.com.mediahub2.activity;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;

import io.vov.vitamio.LibsChecker;
import io.vov.vitamio.MediaPlayer;
import io.vov.vitamio.widget.MediaController;
import io.vov.vitamio.widget.VideoView;
import kaaktaruaa.com.mediahub2.R;

/**
 * Created by muntasir on 6/18/15.
 */
public class VideoPlayerActivity extends Activity{

//    private String pathToFileOrUrl= "http://54.154.10.248:1935/live/ntvbd/playlist.m3u8";
    String pathToFileOrUrl;
    private VideoView mVideoView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_player_activity);

        LibsChecker.checkVitamioLibs(this);
        pathToFileOrUrl = getIntent().getExtras().getString("url");

        mVideoView = (VideoView) findViewById(R.id.surface_view);

        if (pathToFileOrUrl == "") {
            Toast.makeText(this, "Please set the video path for your media file", Toast.LENGTH_LONG).show();
            return;
        } else {

            /*
             * Alternatively,for streaming media you can use
             * mVideoView.setVideoURI(Uri.parse(URLstring));
             */
            mVideoView.setVideoURI(Uri.parse(pathToFileOrUrl));
            mVideoView.setMediaController(new MediaController(this));
            mVideoView.requestFocus();

            mVideoView.start();

           /* mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    // optional need Vitamio 4.0
                    mediaPlayer.setPlaybackSpeed(1.0f);
                    mediaPlayer.start();
                }
            });*/
        }
    }
}
