package kaaktaruaa.com.mediahub2.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;

import java.util.List;

import kaaktaruaa.com.mediahub2.R;
import kaaktaruaa.com.mediahub2.adapter.SeeAllRecyclerAdapter;
import kaaktaruaa.com.mediahub2.datasource.MoviesDataSource;
import kaaktaruaa.com.mediahub2.datasource.bean.SeeAllItem;
import kaaktaruaa.com.mediahub2.datasource.listener.OnSeeAllItemsAvailableListener;
import kaaktaruaa.com.mediahub2.util.Tags;

/**
 * Created by muntasir on 6/15/15.
 */
public class SeeAllActivity extends AppCompatActivity implements OnSeeAllItemsAvailableListener {

    RecyclerView recyclerView;
    SeeAllRecyclerAdapter adapter;
    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.seeall_activity_layout);

        //todo implement getBundle() and generalize this activity for all types of SeeAll view
        MoviesDataSource.getInstance().requestSeeAllItems(this, Tags.LATEST);

        setupToolbar();
        setupViews();

    }

    private void setupToolbar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_seeall);
        setSupportActionBar(toolbar);
        if(getSupportActionBar()!=null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setupViews(){
        progressBar = (ProgressBar) findViewById(R.id.movies_seeall_progressbar);
        recyclerView = (RecyclerView) findViewById(R.id.seeall_recyclerview);

        //show progressbar
        progressBar.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.INVISIBLE);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 3);
        recyclerView.setLayoutManager(mLayoutManager);
        adapter = new SeeAllRecyclerAdapter();
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onSeeAllItemsLoaded(List<SeeAllItem> data) {
        adapter.setDataSet(data);
        progressBar.setVisibility(View.INVISIBLE);
        recyclerView.setVisibility(View.VISIBLE);
    }
}
