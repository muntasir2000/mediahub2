package kaaktaruaa.com.mediahub2.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;

import java.util.ArrayList;
import java.util.List;

import kaaktaruaa.com.mediahub2.R;
import kaaktaruaa.com.mediahub2.adapter.SliderRecyclerAdapter;
import kaaktaruaa.com.mediahub2.datasource.LandingPageDataSource;
import kaaktaruaa.com.mediahub2.datasource.bean.CarouselItem;
import kaaktaruaa.com.mediahub2.datasource.bean.SliderItem;
import kaaktaruaa.com.mediahub2.datasource.listener.OnCarouselItemsAvailableListener;
import kaaktaruaa.com.mediahub2.datasource.listener.OnSlideItemsAvailableListener;
import kaaktaruaa.com.mediahub2.util.Application;
import kaaktaruaa.com.mediahub2.util.Tags;


public class LandingActivity extends AppCompatActivity implements OnSlideItemsAvailableListener, OnCarouselItemsAvailableListener{


    private List<SliderItem> sliderItems;
    private ProgressBar progressBar;
    private RelativeLayout recyclerViewContainer;
    private ProgressBar carouselProgressBar;
    private PagerIndicator carouselIndicator;
    private SliderLayout mainCarousel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Application.getInstance().setContext(this);
        sliderItems = new ArrayList<>();

        setupViews();
        setupCarousel();
     //   setupViewpagers();

        LandingPageDataSource.getInstance().requestAllSliderItems(this);
        LandingPageDataSource.getInstance().requestAllCarouselItems(this);
    }



    private void setupViews(){

        Toolbar toolbar = (Toolbar) findViewById(R.id.landing_page_toolbar);
        setSupportActionBar(toolbar);

        progressBar = (ProgressBar) findViewById(R.id.landing_page_progressbar);
        progressBar.setVisibility(View.VISIBLE);

        recyclerViewContainer = (RelativeLayout) findViewById(R.id.landing_page_viewpagers_container);
        recyclerViewContainer.setVisibility(View.GONE);

        Button liveTvBtn = (Button) findViewById(R.id.landing_page_live_tv_btn);
        Button moviesBtn = (Button) findViewById(R.id.landing_page_movies_button);
        Button shortFilmsButton = (Button) findViewById(R.id.landing_page_short_film_button);
        Button tvShowsButton = (Button) findViewById(R.id.landing_page_tv_shows_button);

        liveTvBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LandingActivity.this, LiveTVDashboardActivity.class);
                startActivity(intent);
            }
        });
        moviesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LandingActivity.this, MoviesDashboardActivity.class);
                startActivity(intent);
            }
        });

    }

    private void setupCarousel(){
        mainCarousel = (SliderLayout) findViewById(R.id.landing_page_carousel);
        carouselProgressBar = (ProgressBar) findViewById(R.id.landing_page_carousel_progressbar);
        carouselIndicator = (PagerIndicator) findViewById(R.id.landing_page_carousel_indicator);

        showProgressbarAndHideCarousel();
    }

    private void showProgressbarAndHideCarousel(){

        //feeling so lazy to remove the carouselprogressbar.
        //so, im just setting it invisible and never showing it
        carouselProgressBar.setVisibility(View.INVISIBLE);
        mainCarousel.setVisibility(View.INVISIBLE);
        carouselIndicator.setVisibility(View.INVISIBLE);
    }

    private void hideProgressbarAndShowCarousel(){
        carouselProgressBar.setVisibility(View.INVISIBLE);
        mainCarousel.setVisibility(View.VISIBLE);
        carouselIndicator.setVisibility(View.VISIBLE);
    }


    private void setupViewpagers(){
        RecyclerView popularRecyclerView = (RecyclerView) findViewById(R.id.popular_movies_tv_show_recyclerview);
        RecyclerView newRecyclerView = (RecyclerView) findViewById(R.id.new_on_mediahub_recyclerview);

        LinearLayoutManager popularRVLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager newRVLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);

        popularRecyclerView.setLayoutManager(popularRVLayoutManager);
        newRecyclerView.setLayoutManager(newRVLayoutManager);
        popularRecyclerView.setAdapter(new SliderRecyclerAdapter(this, sliderItems));
        newRecyclerView.setAdapter(new SliderRecyclerAdapter(this,getNewTaggedSliderItems()));
    }

    private List<SliderItem> getPopularTaggedSliderItems(){
        ArrayList<SliderItem> found = new ArrayList<>();
        for(SliderItem item : sliderItems){
            if(item.getTagsList().contains(Tags.POPULAR)){
                found.add(item);
            }
        }

        return found;
    }

    private List<SliderItem> getNewTaggedSliderItems(){
        ArrayList<SliderItem> found = new ArrayList<>();
        for(SliderItem item : sliderItems){
            if(item.getTagsList().contains(Tags.NEW)){
                found.add(item);
            }
        }

        return found;
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onSlideItemsDataAvailable(List<SliderItem> sliderItems) {
        this.sliderItems = sliderItems;
        setupViewpagers();
        hideProgressbarAndShowContents();
    }

    private void hideProgressbarAndShowContents(){
        progressBar.setVisibility(View.GONE);
        recyclerViewContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void onCarouselItemsAvailable(List<CarouselItem> list) {
        for(CarouselItem item: list){
            TextSliderView mTextSliderView = new TextSliderView(this);
            mTextSliderView
                    .description(item.getName())
                    .image(item.getImageURL())
                    .setScaleType(BaseSliderView.ScaleType.Fit);

            mainCarousel.addSlider(mTextSliderView);
        }

        mainCarousel.setPresetTransformer(SliderLayout.Transformer.Accordion);
        mainCarousel.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mainCarousel.setCustomAnimation(new DescriptionAnimation());
        mainCarousel.setDuration(4000);

        hideProgressbarAndShowCarousel();
    }
}
