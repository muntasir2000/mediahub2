package kaaktaruaa.com.mediahub2.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import kaaktaruaa.com.mediahub2.R;
import kaaktaruaa.com.mediahub2.adapter.SliderRecyclerAdapter;
import kaaktaruaa.com.mediahub2.datasource.MoviesDataSource;
import kaaktaruaa.com.mediahub2.datasource.listener.OnSlideItemsAvailableListener;
import kaaktaruaa.com.mediahub2.datasource.bean.SliderItem;
import kaaktaruaa.com.mediahub2.util.Tags;

/**
 * Created by muntasir on 6/12/15.
 */
public class MoviesGenreFragment extends Fragment implements OnSlideItemsAvailableListener {

    private List<SliderItem> sliderItemList;
    private Context context;
    private LayoutInflater layoutInflater;
    private View view;
    private ProgressBar loadingBar;
    private RelativeLayout contentLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.movies_genre_layout, null);
        layoutInflater = inflater;

        //init the list to avoid NullPointerExceptions
        sliderItemList = new ArrayList<>();

        loadingBar = (ProgressBar) view.findViewById(R.id.play_movie_progressbar);
        contentLayout = (RelativeLayout) view.findViewById(R.id.movies_genre_child_main_contents);

        //hide contents while loading, and show progressbar
        contentLayout.setVisibility(View.GONE);
        loadingBar.setVisibility(View.VISIBLE);

        MoviesDataSource.getInstance().requestAllMoviesList(this);

        return view;
    }

    private void setupViews(){
        RecyclerView actionRecyclerView = (RecyclerView) view.
                findViewById(R.id.movies_genre_action_movies_recyclerview);
        RecyclerView adventureRecyclerView = (RecyclerView) view.
                findViewById(R.id.movies_genre_adventure_movies_recyclerview);
        RecyclerView classicRecyclerView = (RecyclerView) view.
                findViewById(R.id.movies_genre_classic_movies_recyclerview);
        RecyclerView comedyRecyclerView = (RecyclerView) view.
                findViewById(R.id.movies_genre_comedy_movies_recyclerview);
        RecyclerView crimeRecyclerView = (RecyclerView) view.
                findViewById(R.id.movies_genre_crime_movies_recyclerview);
        RecyclerView dramaRecyclerView = (RecyclerView) view.
                findViewById(R.id.movies_genre_drama_movies_recyclerview);
        RecyclerView fantasyRecyclerView = (RecyclerView) view.
                findViewById(R.id.movies_genre_fantasy_movies_recyclerview);

        LinearLayoutManager actionRVLinearLayoutManager = new LinearLayoutManager(context, 
                LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager adventureRVLinearLayoutManager = new LinearLayoutManager(context,
                LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager classicRVLinearLayoutManager = new LinearLayoutManager(context,
                LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager comedyRVLinearLayoutManager = new LinearLayoutManager(context,
                LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager crimeRVLinearLayoutManager = new LinearLayoutManager(context,
                LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager dramaRVLinearLayoutManager = new LinearLayoutManager(context,
                LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager fantacyRVLinearLayoutManager = new LinearLayoutManager(context,
                LinearLayoutManager.HORIZONTAL, false);
        
        actionRecyclerView.setLayoutManager(actionRVLinearLayoutManager);
        adventureRecyclerView.setLayoutManager(adventureRVLinearLayoutManager);
        classicRecyclerView.setLayoutManager(classicRVLinearLayoutManager);
        comedyRecyclerView.setLayoutManager(comedyRVLinearLayoutManager);
        crimeRecyclerView.setLayoutManager(crimeRVLinearLayoutManager);
        dramaRecyclerView.setLayoutManager(dramaRVLinearLayoutManager);
        fantasyRecyclerView.setLayoutManager(fantacyRVLinearLayoutManager);
        
        actionRecyclerView.setAdapter(new SliderRecyclerAdapter(context, getActionMovies()));
        adventureRecyclerView.setAdapter(new SliderRecyclerAdapter(context, getAdventureMovies()));
        classicRecyclerView.setAdapter(new SliderRecyclerAdapter(context, getClassicMovies()));
        comedyRecyclerView.setAdapter(new SliderRecyclerAdapter(context, getComedyMovies()));
        crimeRecyclerView.setAdapter(new SliderRecyclerAdapter(context, getCrimeMovies()));
        dramaRecyclerView.setAdapter(new SliderRecyclerAdapter(context, getDramaMovies()));
        fantasyRecyclerView.setAdapter(new SliderRecyclerAdapter(context, getFantasyMovies()));

    }

    private List<SliderItem> getActionMovies(){
        ArrayList<SliderItem> found = new ArrayList<>();
        for(SliderItem item : sliderItemList){
            if(item.getTagsList().contains(Tags.ACTION)){
                found.add(item);
            }
        }
        return found;
    }

    private List<SliderItem> getAdventureMovies(){
        ArrayList<SliderItem> found = new ArrayList<>();
        for(SliderItem item : sliderItemList){
            if(item.getTagsList().contains(Tags.ADVENTURE)){
                found.add(item);
            }
        }
        return found;
    }

    private List<SliderItem> getClassicMovies(){
        ArrayList<SliderItem> found = new ArrayList<>();
        for(SliderItem item : sliderItemList){
            if(item.getTagsList().contains(Tags.CLASSIC)){
                found.add(item);
            }
        }
        return found;
    }

    private List<SliderItem> getComedyMovies(){
        ArrayList<SliderItem> found = new ArrayList<>();
        for(SliderItem item : sliderItemList){
            if(item.getTagsList().contains(Tags.COMEDY)){
                found.add(item);
            }
        }
        return found;
    }

    private List<SliderItem> getCrimeMovies(){
        ArrayList<SliderItem> found = new ArrayList<>();
        for(SliderItem item : sliderItemList){
            if(item.getTagsList().contains(Tags.CRIME)){
                found.add(item);
            }
        }
        return found;
    }

    private List<SliderItem> getDramaMovies(){
        ArrayList<SliderItem> found = new ArrayList<>();
        for(SliderItem item : sliderItemList){
            if(item.getTagsList().contains(Tags.DRAMA)){
                found.add(item);
            }
        }
        return found;
    }

    private List<SliderItem> getFantasyMovies(){
        ArrayList<SliderItem> found = new ArrayList<>();
        for(SliderItem item : sliderItemList){
            if(item.getTagsList().contains(Tags.FANTASY)){
                found.add(item);
            }
        }
        return found;
    }

    @Override
    public void onSlideItemsDataAvailable(List<SliderItem> sliderItems) {
        this.sliderItemList = sliderItems;
        setupViews();

        loadingBar.setVisibility(View.GONE);
        contentLayout.setVisibility(View.VISIBLE);
    }
}
