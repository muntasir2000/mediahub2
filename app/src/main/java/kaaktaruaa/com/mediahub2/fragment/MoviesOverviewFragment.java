package kaaktaruaa.com.mediahub2.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import kaaktaruaa.com.mediahub2.R;
import kaaktaruaa.com.mediahub2.activity.SeeAllActivity;
import kaaktaruaa.com.mediahub2.adapter.SliderRecyclerAdapter;
import kaaktaruaa.com.mediahub2.datasource.MoviesDataSource;
import kaaktaruaa.com.mediahub2.datasource.listener.OnSlideItemsAvailableListener;
import kaaktaruaa.com.mediahub2.datasource.bean.SliderItem;
import kaaktaruaa.com.mediahub2.util.Tags;

/**
 * Created by muntasir on 6/8/15.
 */
public class MoviesOverviewFragment extends Fragment implements OnSlideItemsAvailableListener {

    private List<SliderItem> sliderItemList;
    private Context context;
    private LayoutInflater layoutInflater;
    private ProgressBar loadingBar;
    private RelativeLayout contentLayout;
    private View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.layoutInflater = inflater;
        this.context = getActivity();

        //init the list to avoid NullPointerExceptions
        sliderItemList = new ArrayList<>();

        view = inflater.inflate(R.layout.movies_overview_layout, null);

        loadingBar = (ProgressBar) view.findViewById(R.id.movies_overview_progressbar);
        contentLayout = (RelativeLayout) view.findViewById(R.id.movies_overview_child_main_contents);

        //hide contents while loading, and show progressbar
        contentLayout.setVisibility(View.GONE);
        loadingBar.setVisibility(View.VISIBLE);

        setupSeeAllButtonListeners(view);

        MoviesDataSource.getInstance().requestAllMoviesList(this);

        return view;
    }

    private void setupSeeAllButtonListeners(View view){
        OnSeeAllClickListener clickListener = new OnSeeAllClickListener();

        TextView latestMovieSeeAll = (TextView) view.findViewById(R.id.movies_overview_latest_movies_seeall);
        TextView freeMoviesSeeAll = (TextView) view.findViewById(R.id.movies_overview_free_movies_seeall);
        TextView premiumMoviesSeeAll = (TextView) view.findViewById(R.id.movies_overview_premium_movies_seeall);
        TextView specialMoviesSeeAll = (TextView) view.findViewById(R.id.movies_overview_special_movies_seeall);

        latestMovieSeeAll.setOnClickListener(clickListener);
        freeMoviesSeeAll.setOnClickListener(clickListener);
        premiumMoviesSeeAll.setOnClickListener(clickListener);
        specialMoviesSeeAll.setOnClickListener(clickListener);
    }

    class OnSeeAllClickListener implements View.OnClickListener{

        @Override
        public void onClick(View view) {
            Intent mIntent = new Intent(view.getContext(), SeeAllActivity.class);

            //todo add more if else conditions
            if(view.getId() == R.id.movies_overview_latest_movies_seeall){
                mIntent.putExtra("tag", Tags.LATEST);
                startActivity(mIntent);
            }
        }
    }

    private void setupViews(){
        RecyclerView latestRecyclerView = (RecyclerView) view.
                findViewById(R.id.movies_overview_latest_movies_recyclerview);
        RecyclerView freeRecyclerView = (RecyclerView) view.
                findViewById(R.id.movies_overview_free_movies_recyclerview);
        RecyclerView premiumRecyclerView = (RecyclerView) view.
                findViewById(R.id.movies_overview_premium_movies_recyclerview);
        RecyclerView specialRecyclerView = (RecyclerView) view.
                findViewById(R.id.movies_overview_special_movies_recyclerview);

        LinearLayoutManager latestRVLinearLayoutManager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager freeRVLinearLayoutManager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager premiumRVLinearLayoutManager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager specialRVLinearLayoutManager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.HORIZONTAL, false);


        latestRecyclerView.setLayoutManager(latestRVLinearLayoutManager);
        freeRecyclerView.setLayoutManager(freeRVLinearLayoutManager);
        premiumRecyclerView.setLayoutManager(premiumRVLinearLayoutManager);
        specialRecyclerView.setLayoutManager(specialRVLinearLayoutManager);

        latestRecyclerView.setAdapter(new SliderRecyclerAdapter(context, getLatestMovies()));
        freeRecyclerView.setAdapter(new SliderRecyclerAdapter(context, getFreeMovies()));
        premiumRecyclerView.setAdapter(new SliderRecyclerAdapter(context, getPremiumMovies()));
        specialRecyclerView.setAdapter(new SliderRecyclerAdapter(context, getSpecialMovies()));
    }

    private List<SliderItem> getLatestMovies(){
        ArrayList<SliderItem> found = new ArrayList<>();
        for(SliderItem item : sliderItemList){
            if(item.getTagsList().contains(Tags.LATEST)){
                found.add(item);
            }
        }

        return found;
    }


    private List<SliderItem> getFreeMovies(){
        ArrayList<SliderItem> found = new ArrayList<>();
        for(SliderItem item : sliderItemList){
            if(item.getTagsList().contains(Tags.FREE)){
                found.add(item);
            }
        }

        return found;
    }


    private List<SliderItem> getPremiumMovies(){
        ArrayList<SliderItem> found = new ArrayList<>();
        for(SliderItem item : sliderItemList){
            if(item.getTagsList().contains(Tags.PREMIUM)){
                found.add(item);
            }
        }

        return found;
    }


    private List<SliderItem> getSpecialMovies(){
        ArrayList<SliderItem> found = new ArrayList<>();
        for(SliderItem item : sliderItemList){
            if(item.getTagsList().contains(Tags.SPECIAL)){
                found.add(item);
            }
        }

        return found;
    }

    @Override
    public void onSlideItemsDataAvailable(List<SliderItem> sliderItems) {
       // loadingDialog.dismiss();
        this.sliderItemList = sliderItems;
        setupViews();

        loadingBar.setVisibility(View.GONE);
        contentLayout.setVisibility(View.VISIBLE);
    }
}
