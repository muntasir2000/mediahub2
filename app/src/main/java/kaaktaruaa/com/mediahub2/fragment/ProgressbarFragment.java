package kaaktaruaa.com.mediahub2.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import kaaktaruaa.com.mediahub2.R;

/**
 * Created by muntasir on 6/8/15.
 */
public class ProgressbarFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.progressbar_fragment_layout, container, false);
        ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.fragment_loading_progressbar);


        return view;
    }
}
