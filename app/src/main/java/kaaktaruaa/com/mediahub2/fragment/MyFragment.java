package kaaktaruaa.com.mediahub2.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import kaaktaruaa.com.mediahub2.R;

/**
 * Created by muntasir on 6/9/15.
 */
public class MyFragment extends Fragment {
    private String data;
    private View view;
    ProgressBar progressBar;
    ProgressDialog progressDialog;
    TextView textView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_layout, null, false);
        return view;
    }

}
