package kaaktaruaa.com.mediahub2.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;

import io.vov.vitamio.utils.Log;
import kaaktaruaa.com.mediahub2.R;
import kaaktaruaa.com.mediahub2.activity.PlayMovieActivity;
import kaaktaruaa.com.mediahub2.datasource.bean.SliderItem;
import kaaktaruaa.com.mediahub2.util.Constant;

/**
 * Created by muntasir on 6/28/15.
 */
public class SliderRecyclerAdapter extends RecyclerView.Adapter<SliderRecyclerAdapter.SliderItemViewHolder>{

    private List<SliderItem> sliderItemList;
    private Context context;
    public SliderRecyclerAdapter(Context context, List<SliderItem> list) {
        this.sliderItemList = list;
        this.context = context;

    }

    @Override
    public SliderItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(parent.getContext(), R.layout.slider_item_new, null);
        return new SliderItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SliderItemViewHolder holder, int position) {
        Picasso.with(context).load(sliderItemList.get(position).getThumbUrl())
             //   .resize(R.dimen.slider_item_card_width, R.dimen.slider_item_card_height)
                .fit()
                .placeholder(R.drawable.image_placeholder)
                .into(holder.imageView);

        holder.imageView.setTag(sliderItemList.get(position).getId());
        holder.imageView.setOnClickListener(new OnSliderItemClickedListener());
    }

    @Override
    public int getItemCount() {
        return sliderItemList.size();
    }

    public static class SliderItemViewHolder extends RecyclerView.ViewHolder{

        ImageView imageView;
        public SliderItemViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.slider_item_imageview);
        }
    }

    class OnSliderItemClickedListener implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            String id = v.getTag().toString();
            Intent intent = new Intent(v.getContext(), PlayMovieActivity.class);
            intent.putExtra("id", id);
            v.getContext().startActivity(intent);
        }
    }
}
