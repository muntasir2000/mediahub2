package kaaktaruaa.com.mediahub2.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import kaaktaruaa.com.mediahub2.R;
import kaaktaruaa.com.mediahub2.datasource.bean.SeeAllItem;

/**
 * Created by muntasir on 6/15/15.
 */
public class SeeAllRecyclerAdapter extends RecyclerView.Adapter<SeeAllRecyclerAdapter.SeeAllViewHolder> {

    private Context context;
    private List<SeeAllItem> itemsList;

    public SeeAllRecyclerAdapter(){
        itemsList = new ArrayList<>();
    }

    public SeeAllRecyclerAdapter(List<SeeAllItem> list){
        this.itemsList = list;
    }

    @Override
    public SeeAllRecyclerAdapter.SeeAllViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        context = viewGroup.getContext();
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.seeall_item_card, viewGroup, false);

        return new SeeAllViewHolder(v);
    }

    @Override
    public void onBindViewHolder(SeeAllRecyclerAdapter.SeeAllViewHolder viewHolder, int i) {
        Picasso.with(context).load(itemsList.get(i).getThumbUrl())
                .into(viewHolder.posterImageView);

        viewHolder.titleTextView.setText(itemsList.get(i).getTitle());
        viewHolder.wishListCheckBox.setChecked(itemsList.get(i).isInWishList());
        viewHolder.ratingBar.setRating(itemsList.get(i).getRating());
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public void setDataSet(List<SeeAllItem> dataSet){
        itemsList = dataSet;
        notifyDataSetChanged();
    }


    public static class SeeAllViewHolder extends RecyclerView.ViewHolder{
        ImageView posterImageView;
        TextView titleTextView;
        RatingBar ratingBar;
        CheckBox wishListCheckBox;

        public SeeAllViewHolder(View itemView) {
            super(itemView);
            posterImageView = (ImageView) itemView.findViewById(R.id.seeall_card_poster);
            titleTextView = (TextView) itemView.findViewById(R.id.seeall_card_title_textview);
            ratingBar = (RatingBar) itemView.findViewById(R.id.seeall_card_ratingbar);
            wishListCheckBox = (CheckBox) itemView.findViewById(R.id.seeall_card_wish_checkbox);
        }
    }
}
