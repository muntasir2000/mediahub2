package kaaktaruaa.com.mediahub2.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import kaaktaruaa.com.mediahub2.R;
import kaaktaruaa.com.mediahub2.activity.VideoPlayerActivity;
import kaaktaruaa.com.mediahub2.datasource.bean.SliderItem;
import kaaktaruaa.com.mediahub2.util.Constant;

/**
 * Created by muntasir on 6/8/15.
 */
@Deprecated
public class SliderPagerAdapter extends PagerAdapter {

    private Context context;
    private LayoutInflater layoutInflater;
    private List<SliderItem> sliderItemList;

    public SliderPagerAdapter(Context context, LayoutInflater layoutInflater, List<SliderItem> sliderItemList){
        this.context = context;
        this.layoutInflater = layoutInflater;
        this.sliderItemList = sliderItemList;

    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View page =
                layoutInflater.inflate(R.layout.slider_item, container, false);

   //     TextView titleTextView = (TextView) page.findViewById(R.id.slide_text);
        ImageView thumbImageView = (ImageView) page.findViewById(R.id.imageView);

   //     titleTextView.setText(sliderItemList.get(position).getTitle());

        Picasso.with(context).setLoggingEnabled(true);
        Picasso.with(context).load(sliderItemList.get(position).getThumbUrl())
                .fit()
                .placeholder(R.drawable.thumb_empty)
                .into(thumbImageView);

        Log.d(Constant.DEBUG_TAG, sliderItemList.get(position).getThumbUrl());

        container.addView(page);
        page.setOnClickListener(new OnSliderItemClickListener(sliderItemList.get(position)));

        return (page);
    }

    @Override
    public void destroyItem(ViewGroup container, int position,
                            Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return sliderItemList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }

    @Override
    public float getPageWidth(int position) {
        return .33f;
    }
}

class OnSliderItemClickListener implements View.OnClickListener{

    SliderItem item;
    public OnSliderItemClickListener(SliderItem item){
        this.item = item;
    }

    @Override
    public void onClick(View view) {
        if(item.getStreamUrl() != null){
            Intent intent = new Intent(view.getContext(), VideoPlayerActivity.class);
            intent.putExtra("url", item.getStreamUrl());
            view.getContext().startActivity(intent);

        }
        Toast.makeText(view.getContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
    }
}
