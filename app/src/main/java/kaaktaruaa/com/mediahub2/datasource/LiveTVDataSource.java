package kaaktaruaa.com.mediahub2.datasource;

import java.util.List;

import kaaktaruaa.com.mediahub2.datasource.asynctask.LiveTVSliderItemsLoaderAsyncTask;
import kaaktaruaa.com.mediahub2.datasource.bean.SliderItem;
import kaaktaruaa.com.mediahub2.datasource.listener.OnSliderItemsLoadedListener;
import kaaktaruaa.com.mediahub2.datasource.listener.OnSlideItemsAvailableListener;

/**
 * Created by muntasir on 6/18/15.
 */
public class LiveTVDataSource implements OnSliderItemsLoadedListener {
    private static LiveTVDataSource instance = new LiveTVDataSource();

    private List<SliderItem> sliderItemsCache;

    public static LiveTVDataSource getInstance() {
        return instance;
    }

    private LiveTVDataSource() {
    }

    public void requestAllSliderItems(OnSlideItemsAvailableListener caller){
        if(sliderItemsCache == null){
            new LiveTVSliderItemsLoaderAsyncTask(this, caller).execute();
        }else
            caller.onSlideItemsDataAvailable(sliderItemsCache);
    }


    @Override
    public void onAllSliderItemsLoaded(List<SliderItem> data) {
        this.sliderItemsCache = data;
    }
}
