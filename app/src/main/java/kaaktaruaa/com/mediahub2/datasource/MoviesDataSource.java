package kaaktaruaa.com.mediahub2.datasource;

import java.util.List;

import kaaktaruaa.com.mediahub2.datasource.asynctask.MoviesDetailsLoaderAsyncTask;
import kaaktaruaa.com.mediahub2.datasource.asynctask.MoviesSliderItemsLoaderAsyncTask;
import kaaktaruaa.com.mediahub2.datasource.asynctask.MoviesSeeAllItemsLoaderAsyncTask;
import kaaktaruaa.com.mediahub2.datasource.bean.SliderItem;
import kaaktaruaa.com.mediahub2.datasource.listener.OnMovieDetailsAvailableListener;
import kaaktaruaa.com.mediahub2.datasource.listener.OnSliderItemsLoadedListener;
import kaaktaruaa.com.mediahub2.datasource.listener.OnSeeAllItemsAvailableListener;
import kaaktaruaa.com.mediahub2.datasource.listener.OnSlideItemsAvailableListener;

/**
 * Created by muntasir on 6/7/15.
 */

/*
* MoviesDataSource class keeps cache of all movies slider items in memory for faster access.
* But no cache is kept for 'See All' activities */

public class MoviesDataSource implements OnSliderItemsLoadedListener {
    private static MoviesDataSource instance;
    private List<SliderItem> allMoviesList;

    public static MoviesDataSource getInstance() {
        if (instance == null)
            instance = new MoviesDataSource();
        return instance;
    }

    private MoviesDataSource() {
    }


    public void requestAllMoviesList(OnSlideItemsAvailableListener caller) {

        if (allMoviesList == null) //no cached data available

            //the caller will be notified from the AsyncTask. No need to worry
            //about notifying from here.
            new MoviesSliderItemsLoaderAsyncTask(this, caller).execute();

        else
            //no need to fetch from network. its already available
            caller.onSlideItemsDataAvailable(allMoviesList);
    }


    public void requestSeeAllItems(OnSeeAllItemsAvailableListener caller, int type){
        new MoviesSeeAllItemsLoaderAsyncTask(caller, type).execute();
    }

    public void requestMovieDetails(OnMovieDetailsAvailableListener caller, String id){
        new MoviesDetailsLoaderAsyncTask(caller, id).execute();
    }

    @Override
    public void onAllSliderItemsLoaded(List<SliderItem> data) {
        this.allMoviesList = data;
    }
}
