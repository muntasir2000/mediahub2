package kaaktaruaa.com.mediahub2.datasource.asynctask;

import android.os.AsyncTask;

import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.MobileServiceList;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;

import java.net.MalformedURLException;
import java.util.concurrent.ExecutionException;

import io.vov.vitamio.utils.Log;
import kaaktaruaa.com.mediahub2.datasource.bean.Movie;
import kaaktaruaa.com.mediahub2.datasource.listener.OnMovieDetailsAvailableListener;
import kaaktaruaa.com.mediahub2.util.Application;
import kaaktaruaa.com.mediahub2.util.Constant;

/**
 * Created by muntasir on 7/2/15.
 */
public class MoviesDetailsLoaderAsyncTask extends AsyncTask<Void, Void, Movie> {
    private OnMovieDetailsAvailableListener caller;
    String movieID;

    public MoviesDetailsLoaderAsyncTask(OnMovieDetailsAvailableListener caller, String movieID) {
        this.caller = caller;
        this.movieID = movieID;

        Log.e(Constant.DEBUG_TAG, "Movie id = "+movieID);
    }

    @Override
    protected Movie doInBackground(Void... params) {
       /* Movie movie = new Movie();
        movie.setTitle("Intersteller");
        movie.setDescription("A team of explorers travel through a wormhole in space in an attempt to ensure humanity's survival.");
        movie.setStreamUrl("http://mediahub2streamer.streaming.mediaservices.windows.net/830f7efb-ae34-44a8-8b3a-489ddb3ae664/Wanderers%20-%20a%20short%20film%20by%20Erik%20Wernquist-HD.ism/Manifest(format=m3u8-aapl-v3)");
        movie.setStarringActorNamesListJson("Stars: Matthew McConaughey, Anne Hathaway, Jessica Chastain");
        movie.setRating(4.4f);
        movie.setReleaseYear(2014);

        return movie;*/

        Movie movie = null;
        try {
            MobileServiceClient client = new MobileServiceClient(Constant.AZURE_APPURL, Constant.AZURE_APPKEY,
                    Application.getInstance().getContext());
            MobileServiceTable<Movie> movieTable = client.getTable("movie", Movie.class);
            MobileServiceList<Movie> responseData = movieTable.where().field("id").eq(movieID).
                    select("id", "title", "description", "streamUrl",
                            "starringActorsListJson", "releaseYear", "rating")
                    .execute().get();
            movie = responseData.get(0);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return movie;
    }

    @Override
    protected void onPostExecute(Movie movie) {
        caller.onDetailsLoaded(movie);
    }
}
