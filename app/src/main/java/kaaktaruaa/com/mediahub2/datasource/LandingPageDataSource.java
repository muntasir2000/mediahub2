package kaaktaruaa.com.mediahub2.datasource;

import com.microsoft.windowsazure.mobileservices.MobileServiceClient;

import java.net.MalformedURLException;
import java.util.List;

import kaaktaruaa.com.mediahub2.datasource.asynctask.CarouselItemsLoaderAsyncTask;
import kaaktaruaa.com.mediahub2.datasource.asynctask.LandingPageSliderItemsLoaderAsyncTask;
import kaaktaruaa.com.mediahub2.datasource.bean.CarouselItem;
import kaaktaruaa.com.mediahub2.datasource.bean.SliderItem;
import kaaktaruaa.com.mediahub2.datasource.listener.OnCarouselItemsAvailableListener;
import kaaktaruaa.com.mediahub2.datasource.listener.OnCarouselItemsLoadedListener;
import kaaktaruaa.com.mediahub2.datasource.listener.OnSliderItemsLoadedListener;
import kaaktaruaa.com.mediahub2.datasource.listener.OnSlideItemsAvailableListener;
import kaaktaruaa.com.mediahub2.util.Application;
import kaaktaruaa.com.mediahub2.util.Constant;

/**
 * Created by muntasir on 6/17/15.
 */
public class LandingPageDataSource implements OnSliderItemsLoadedListener, OnCarouselItemsLoadedListener {

    private MobileServiceClient mobileServiceClient;

    private static LandingPageDataSource instance;
    private List<SliderItem> allLandingPageItemsCache;
    private List<CarouselItem> allCarouselItemsCache;

    private LandingPageDataSource(){
        try {
            mobileServiceClient = new MobileServiceClient(Constant.AZURE_APPURL, Constant.AZURE_APPKEY, Application.getInstance().getContext());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public static LandingPageDataSource getInstance() {
        if(instance == null)
            instance = new LandingPageDataSource();
        return instance;
    }


    public void requestAllSliderItems(OnSlideItemsAvailableListener caller){
        if(allLandingPageItemsCache == null)
            new LandingPageSliderItemsLoaderAsyncTask(this, caller).execute();
        else
            caller.onSlideItemsDataAvailable(allLandingPageItemsCache);
    }

    @Override
    public void onAllSliderItemsLoaded(List<SliderItem> data) {
        this.allLandingPageItemsCache = data;
    }

    public void requestAllCarouselItems(OnCarouselItemsAvailableListener caller){
        if(allCarouselItemsCache == null){
            new CarouselItemsLoaderAsyncTask(this, caller, mobileServiceClient).execute();
        }
    }


    @Override
    public void onCarouselItemsLoaded(List<CarouselItem> list) {
        allCarouselItemsCache = list;
    }
}
