package kaaktaruaa.com.mediahub2.datasource.listener;

import java.util.List;

import kaaktaruaa.com.mediahub2.datasource.bean.SliderItem;

/**
 * Created by muntasir on 6/15/15.
 */
public interface OnSliderItemsLoadedListener {
    void onAllSliderItemsLoaded(List<SliderItem> data);
}