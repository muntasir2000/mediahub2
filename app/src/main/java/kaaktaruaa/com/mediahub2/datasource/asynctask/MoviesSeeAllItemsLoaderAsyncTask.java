package kaaktaruaa.com.mediahub2.datasource.asynctask;

import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

import kaaktaruaa.com.mediahub2.datasource.bean.SeeAllItem;
import kaaktaruaa.com.mediahub2.datasource.listener.OnSeeAllItemsAvailableListener;

/**
 * Created by muntasir on 6/15/15.
 */
public class MoviesSeeAllItemsLoaderAsyncTask extends AsyncTask<Void, Void, List<SeeAllItem>> {

    private OnSeeAllItemsAvailableListener caller;
    public MoviesSeeAllItemsLoaderAsyncTask(OnSeeAllItemsAvailableListener caller, int type){
        this.caller = caller;
    }

    @Override
    protected List<SeeAllItem> doInBackground(Void... voids) {
        //todo implement type
        ArrayList<SeeAllItem> list = new ArrayList<>();
        SeeAllItem item = new SeeAllItem();
        item.setTitle("Grey");

        item.setThumbUrl("http://gdj.gdj.netdna-cdn.com/wp-content/uploads/2011/12/grey-movie-poster.jpg");
        list.add(item);

        item = new SeeAllItem();
        item.setTitle("Star Trek");

        item.setThumbUrl("http://www.theflea.co.nz/wp-content/uploads/2010/02/star-trek-movie-poster.jpg");
        list.add(item);

        item = new SeeAllItem();
        item.setTitle("Sherlock Holms");
        item.setThumbUrl("http://images5.fanpop.com/image/photos/26400000/New-Movie-Poster-sherlock-holmes-a-game-of-shadows-26499306-453-660.jpg");

        item = new SeeAllItem();
        item.setTitle("Titanic");
        item.setThumbUrl("http://images.moviepostershop.com/titanic-movie-poster-1997-1020339699.jpg");
        list.add(item);

        item = new SeeAllItem();
        item.setTitle("Angels and Demons");
        item.setThumbUrl("http://www.cyber-cinema.com/original/angeldemonsOrg.jpg");
        list.add(item);

        item = new SeeAllItem();
        item.setTitle("127 Hours");
        item.setThumbUrl("http://gdj.gdj.netdna-cdn.com/wp-content/uploads/2011/03/127-hours-movie-poster.jpg");
        list.add(item);

        item = new SeeAllItem();
        item.setTitle("Gamer");
        item.setThumbUrl("https://blackwhiteandredposters.files.wordpress.com/2010/11/gamer-movie-poster.jpg");
        list.add(item);

        item = new SeeAllItem();
        item.setTitle("Star Trek");

        item.setThumbUrl("http://www.theflea.co.nz/wp-content/uploads/2010/02/star-trek-movie-poster.jpg");
        list.add(item);

        item = new SeeAllItem();
        item.setTitle("Sherlock Holms");
        item.setThumbUrl("http://images5.fanpop.com/image/photos/26400000/New-Movie-Poster-sherlock-holmes-a-game-of-shadows-26499306-453-660.jpg");

        item = new SeeAllItem();
        item.setTitle("Titanic");
        item.setThumbUrl("http://images.moviepostershop.com/titanic-movie-poster-1997-1020339699.jpg");
        list.add(item);

        item = new SeeAllItem();
        item.setTitle("Angels and Demons");
        item.setThumbUrl("http://www.cyber-cinema.com/original/angeldemonsOrg.jpg");
        list.add(item);

        item = new SeeAllItem();
        item.setTitle("127 Hours");
        item.setThumbUrl("http://gdj.gdj.netdna-cdn.com/wp-content/uploads/2011/03/127-hours-movie-poster.jpg");
        list.add(item);

        item = new SeeAllItem();
        item.setTitle("Gamer");
        item.setThumbUrl("https://blackwhiteandredposters.files.wordpress.com/2010/11/gamer-movie-poster.jpg");
        list.add(item);

        item = new SeeAllItem();
        item.setTitle("Star Trek");

        item.setThumbUrl("http://www.theflea.co.nz/wp-content/uploads/2010/02/star-trek-movie-poster.jpg");
        list.add(item);

        item = new SeeAllItem();
        item.setTitle("Sherlock Holms");
        item.setThumbUrl("http://images5.fanpop.com/image/photos/26400000/New-Movie-Poster-sherlock-holmes-a-game-of-shadows-26499306-453-660.jpg");

        item = new SeeAllItem();
        item.setTitle("Titanic");
        item.setThumbUrl("http://images.moviepostershop.com/titanic-movie-poster-1997-1020339699.jpg");
        list.add(item);

        item = new SeeAllItem();
        item.setTitle("Angels and Demons");
        item.setThumbUrl("http://www.cyber-cinema.com/original/angeldemonsOrg.jpg");
        list.add(item);

        item = new SeeAllItem();
        item.setTitle("127 Hours");
        item.setThumbUrl("http://gdj.gdj.netdna-cdn.com/wp-content/uploads/2011/03/127-hours-movie-poster.jpg");
        list.add(item);

        item = new SeeAllItem();
        item.setTitle("Gamer");
        item.setThumbUrl("https://blackwhiteandredposters.files.wordpress.com/2010/11/gamer-movie-poster.jpg");
        list.add(item);

        try {
            Thread.sleep(6000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return list;
    }

    @Override
    protected void onPostExecute(List<SeeAllItem> seeAllItems) {
        caller.onSeeAllItemsLoaded(seeAllItems);
    }
}
