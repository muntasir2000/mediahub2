package kaaktaruaa.com.mediahub2.datasource.asynctask;

import android.os.AsyncTask;

import com.microsoft.windowsazure.mobileservices.MobileServiceClient;

import java.util.ArrayList;
import java.util.List;

import kaaktaruaa.com.mediahub2.datasource.bean.CarouselItem;
import kaaktaruaa.com.mediahub2.datasource.listener.OnCarouselItemsAvailableListener;
import kaaktaruaa.com.mediahub2.datasource.listener.OnCarouselItemsLoadedListener;

/**
 * Created by muntasir on 6/21/15.
 */
public class CarouselItemsLoaderAsyncTask extends AsyncTask<Void, Void, List<CarouselItem>> {

    private OnCarouselItemsAvailableListener callerActivity;
    private OnCarouselItemsLoadedListener cacheDestination;
    private MobileServiceClient mobileServiceClient;
    public CarouselItemsLoaderAsyncTask(OnCarouselItemsLoadedListener cacheDestination,
                                        OnCarouselItemsAvailableListener callerActivity,
                                        MobileServiceClient client) {
        this.callerActivity = callerActivity;
        this.cacheDestination = cacheDestination;
        mobileServiceClient = client;
    }

    @Override
    protected List<CarouselItem> doInBackground(Void... params) {



        List<CarouselItem> result = new ArrayList<>();

        CarouselItem carouselItem = new CarouselItem();
        carouselItem.setImageURL("http://tamilgun.com/wp-content/uploads/2015/05/mad-max-fury-road.jpg");
        carouselItem.setName("Mad Max - Fury Road");

        result.add(carouselItem);

        carouselItem = new CarouselItem();
        carouselItem.setImageURL("http://images.latinpost.com/data/images/full/27886/jurassic-world.jpg?w=600");
        carouselItem.setName("Jurrasic World");

        result.add(carouselItem);

        carouselItem = new CarouselItem();
        carouselItem.setImageURL("http://entertainment.iafrica.com/assets/13/3704/294697/2116161.JPEG");
        carouselItem.setName("San Andreas");

        result.add(carouselItem);

        carouselItem = new CarouselItem();
        carouselItem.setImageURL("https://wattsupwiththat.files.wordpress.com/2015/06/tomorrowland-movie.jpg?w=720");
        carouselItem.setName("Tomorrowland");

        result.add(carouselItem);

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    protected void onPostExecute(List<CarouselItem> carouselItems) {
        callerActivity.onCarouselItemsAvailable(carouselItems);
        cacheDestination.onCarouselItemsLoaded(carouselItems);
    }
}
