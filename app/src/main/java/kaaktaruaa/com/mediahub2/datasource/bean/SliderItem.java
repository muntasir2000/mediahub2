package kaaktaruaa.com.mediahub2.datasource.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by muntasir on 6/7/15.
 */
public class SliderItem {

    private String id;
    private String title;
    private String thumbUrl;
    private String tagsJson;
    private List<Integer> tagsList;
    private String streamUrl;
    public SliderItem(){}



    public SliderItem(String title, String thumbUrl, List<Integer> tags){
        this.title = title;
        this.thumbUrl = thumbUrl;
        this.tagsList = tags;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumbUrl() {
        return thumbUrl;
    }

    public void setThumbUrl(String thumbUrl) {
        this.thumbUrl = thumbUrl;
    }

    public List<Integer> getTagsList() {
        return tagsList;
    }

    public void setTagsList(List<Integer> tagsList) {
        this.tagsList = tagsList;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTagsJson() {
        return tagsJson;
    }

    public void setTagsJson(String tagsJson) {
        this.tagsJson = tagsJson;
    }

    public String getStreamUrl() {
        return streamUrl;
    }

    public void setStreamUrl(String streamUrl) {
        this.streamUrl = streamUrl;
    }
}
