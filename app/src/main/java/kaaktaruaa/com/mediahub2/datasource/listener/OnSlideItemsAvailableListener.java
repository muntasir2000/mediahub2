package kaaktaruaa.com.mediahub2.datasource.listener;

import java.util.List;

import kaaktaruaa.com.mediahub2.datasource.bean.SliderItem;

/**
 * Created by muntasir on 6/7/15.
 */
public interface OnSlideItemsAvailableListener {
    public void onSlideItemsDataAvailable(List<SliderItem> sliderItems);
}
