package kaaktaruaa.com.mediahub2.datasource.bean;

/**
 * Created by muntasir on 6/15/15.
 */
public class SeeAllItem {
    private String thumbUrl;
    private String title;
    //todo isInWishlist not implemented in azure
    private boolean isInWishList = false;
    private float rating = 4.5f;
    //todo remove default rating and wishlist

    //These are not used
    private String description;
    private boolean isFree;
    private  boolean isHD;

    public String getThumbUrl() {
        return thumbUrl;
    }

    public void setThumbUrl(String thumbUrl) {
        this.thumbUrl = thumbUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isHD() {
        return isHD;
    }

    public void setIsHD(boolean isHD) {
        this.isHD = isHD;
    }

    public boolean isFree() {
        return isFree;
    }

    public void setIsFree(boolean isFree) {
        this.isFree = isFree;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public boolean isInWishList() {
        return isInWishList;
    }

    public void setIsInWishList(boolean isInWishList) {
        this.isInWishList = isInWishList;
    }
}
