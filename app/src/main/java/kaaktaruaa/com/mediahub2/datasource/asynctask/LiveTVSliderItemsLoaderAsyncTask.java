package kaaktaruaa.com.mediahub2.datasource.asynctask;

import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

import kaaktaruaa.com.mediahub2.datasource.bean.SliderItem;
import kaaktaruaa.com.mediahub2.datasource.listener.OnSliderItemsLoadedListener;
import kaaktaruaa.com.mediahub2.datasource.listener.OnSlideItemsAvailableListener;
import kaaktaruaa.com.mediahub2.util.Tags;

/**
 * Created by muntasir on 6/18/15.
 */
public class LiveTVSliderItemsLoaderAsyncTask extends AsyncTask<Void, Void, List<SliderItem>> {

    OnSliderItemsLoadedListener cacheDestination;
    OnSlideItemsAvailableListener callerFragment;

    public LiveTVSliderItemsLoaderAsyncTask(OnSliderItemsLoadedListener cacheDestination, OnSlideItemsAvailableListener callerFragment){
        this.cacheDestination = cacheDestination;
        this.callerFragment = callerFragment;
    }

    @Override
    protected List<SliderItem> doInBackground(Void... voids) {
        List<SliderItem> list = new ArrayList<>();

        SliderItem sliderItem = new SliderItem();
        sliderItem.setTitle("NTV");
        sliderItem.setThumbUrl("http://www.jagobd.com/wp-content/uploads/2012/09/303008_222340827839563_209773862429593_573000_2067213539_n.jpg");
        sliderItem.setStreamUrl("http://54.154.10.248:1935/live/ntvbd/playlist.m3u8");
        List<Integer> tags = new ArrayList<Integer>();
        tags.add(Tags.GENERAL);
        sliderItem.setTagsList(tags);

        list.add(sliderItem);


        sliderItem = new SliderItem();
        sliderItem.setTitle("Nasa TV");
        sliderItem.setThumbUrl("http://onlinetv24-7.com/wp-content/uploads/2015/02/Nasa-Tv.png");
        sliderItem.setStreamUrl("http://www.nasa.gov/multimedia/nasatv/NTV-Public-IPS.m3u8");
        tags = new ArrayList<Integer>();
        tags.add(Tags.GENERAL);
        sliderItem.setTagsList(tags);

        list.add(sliderItem);

        sliderItem = new SliderItem();
        sliderItem.setTitle("Demo Channel");
        sliderItem.setStreamUrl("http://srv6.zoeweb.tv:1935/z330-live/stream/playlist.m3u8");
        sliderItem.setThumbUrl("http://d0.awsstatic.com/logos/customers/wowza-streaming-engine.jpg");
        tags = new ArrayList<Integer>();
        tags.add(Tags.GENERAL);
        sliderItem.setTagsList(tags);
        list.add(sliderItem);

        sliderItem = new SliderItem();
        sliderItem.setTitle("VEVO Nonstop");
        sliderItem.setStreamUrl("http://vevoplaylist-live.hls.adaptive.level3.net/vevo/ch1/appleman.m3u8");
        sliderItem.setThumbUrl("https://bandt-au.s3.amazonaws.com/information/uploads/2014/12/VEVO-Android-App-For-Music.png?544097");
        tags = new ArrayList<Integer>();
        tags.add(Tags.ENTERTAINMENT);
        sliderItem.setTagsList(tags);
        list.add(sliderItem);



        sliderItem = new SliderItem();
        sliderItem.setTitle("Nasa TV");
        sliderItem.setThumbUrl("http://onlinetv24-7.com/wp-content/uploads/2015/02/Nasa-Tv.png");
        sliderItem.setStreamUrl("http://www.nasa.gov/multimedia/nasatv/NTV-Public-IPS.m3u8");
        tags = new ArrayList<Integer>();
        tags.add(Tags.NEWS);
        sliderItem.setTagsList(tags);

        list.add(sliderItem);

        sliderItem = new SliderItem();
        sliderItem.setTitle("Demo Channel");
        sliderItem.setStreamUrl("http://srv6.zoeweb.tv:1935/z330-live/stream/playlist.m3u8");
        sliderItem.setThumbUrl("http://d0.awsstatic.com/logos/customers/wowza-streaming-engine.jpg");
        tags = new ArrayList<Integer>();
        tags.add(Tags.NEWS);
        sliderItem.setTagsList(tags);
        list.add(sliderItem);

        sliderItem = new SliderItem();
        sliderItem.setTitle("VEVO Nonstop");
        sliderItem.setStreamUrl("http://vevoplaylist-live.hls.adaptive.level3.net/vevo/ch1/appleman.m3u8");
        sliderItem.setThumbUrl("https://bandt-au.s3.amazonaws.com/information/uploads/2014/12/VEVO-Android-App-For-Music.png?544097");
        tags = new ArrayList<Integer>();
        tags.add(Tags.NEWS);
        sliderItem.setTagsList(tags);
        list.add(sliderItem);

        sliderItem = new SliderItem();
        sliderItem.setTitle("Nasa TV");
        sliderItem.setThumbUrl("http://onlinetv24-7.com/wp-content/uploads/2015/02/Nasa-Tv.png");
        sliderItem.setStreamUrl("http://www.nasa.gov/multimedia/nasatv/NTV-Public-IPS.m3u8");
        tags = new ArrayList<Integer>();
        tags.add(Tags.NEWS);
        sliderItem.setTagsList(tags);

        list.add(sliderItem);

        sliderItem = new SliderItem();
        sliderItem.setTitle("Demo Channel");
        sliderItem.setStreamUrl("http://srv6.zoeweb.tv:1935/z330-live/stream/playlist.m3u8");
        sliderItem.setThumbUrl("http://d0.awsstatic.com/logos/customers/wowza-streaming-engine.jpg");
        tags = new ArrayList<Integer>();
        tags.add(Tags.NEWS);
        sliderItem.setTagsList(tags);
        list.add(sliderItem);

        sliderItem = new SliderItem();
        sliderItem.setTitle("VEVO Nonstop");
        sliderItem.setStreamUrl("http://vevoplaylist-live.hls.adaptive.level3.net/vevo/ch1/appleman.m3u8");
        sliderItem.setThumbUrl("https://bandt-au.s3.amazonaws.com/information/uploads/2014/12/VEVO-Android-App-For-Music.png?544097");
        tags = new ArrayList<Integer>();
        tags.add(Tags.NEWS);
        sliderItem.setTagsList(tags);
        list.add(sliderItem);

        //simulate network delay
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        return list;
    }

    @Override
    protected void onPostExecute(List<SliderItem> sliderItems) {
        cacheDestination.onAllSliderItemsLoaded(sliderItems);
        callerFragment.onSlideItemsDataAvailable(sliderItems);
    }
}
