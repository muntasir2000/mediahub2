package kaaktaruaa.com.mediahub2.datasource.listener;

import java.util.List;

import kaaktaruaa.com.mediahub2.datasource.bean.CarouselItem;

/**
 * Created by muntasir on 6/21/15.
 */
public interface OnCarouselItemsLoadedListener {
    void onCarouselItemsLoaded(List<CarouselItem> list);
}
