package kaaktaruaa.com.mediahub2.datasource.listener;

import kaaktaruaa.com.mediahub2.datasource.bean.Movie;

/**
 * Created by muntasir on 7/2/15.
 */
public interface OnMovieDetailsAvailableListener {
    void onDetailsLoaded(Movie movie);
}
