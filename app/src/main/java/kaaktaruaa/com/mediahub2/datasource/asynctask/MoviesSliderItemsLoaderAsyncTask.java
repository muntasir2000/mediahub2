package kaaktaruaa.com.mediahub2.datasource.asynctask;

import android.os.AsyncTask;

import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.MobileServiceList;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import io.vov.vitamio.utils.Log;
import kaaktaruaa.com.mediahub2.datasource.listener.OnSliderItemsLoadedListener;
import kaaktaruaa.com.mediahub2.datasource.listener.OnSlideItemsAvailableListener;
import kaaktaruaa.com.mediahub2.datasource.bean.SliderItem;
import kaaktaruaa.com.mediahub2.util.Application;
import kaaktaruaa.com.mediahub2.util.Constant;
import kaaktaruaa.com.mediahub2.util.Tags;

/**
 * Created by muntasir on 6/11/15.
 */
public class MoviesSliderItemsLoaderAsyncTask extends AsyncTask<Void, Void, List<SliderItem>> {

    private OnSliderItemsLoadedListener dataSource;
    private OnSlideItemsAvailableListener caller;

    public MoviesSliderItemsLoaderAsyncTask(OnSliderItemsLoadedListener cacheDestination, OnSlideItemsAvailableListener caller){
        this.dataSource = cacheDestination;
        this.caller = caller;
    }

    @Override
    protected List<SliderItem> doInBackground(Void... voids) {
        ArrayList<SliderItem> list = new ArrayList<>();

        /*SliderItem mSliderItem = new SliderItem();
        mSliderItem.setTitle("Project Almanac");
        mSliderItem.setThumbUrl("http://ia.media-imdb.com/images/M/MV5BMTUxMjQ2NjI4OV5BMl5BanBnXkFtZTgwODc2NjUwNDE@._V1_SX214_AL_.jpg");
        List<Integer> tags = new ArrayList<Integer>();
        tags.add(Tags.LATEST);
        tags.add(Tags.ADVENTURE);
        mSliderItem.setTagsList(tags);

        list.add(mSliderItem);



        mSliderItem = new SliderItem();
        mSliderItem.setTitle("Avengers");
        mSliderItem.setThumbUrl("http://www.iceposter.com/thumbs/MOV_575e1c88_b.jpg");
        tags = new ArrayList<Integer>();
        tags.add(Tags.LATEST);
        tags.add(Tags.ACTION);
        mSliderItem.setTagsList(tags);

        list.add(mSliderItem);



        mSliderItem = new SliderItem();
        mSliderItem.setTitle("Chappie");
        mSliderItem.setThumbUrl("http://fc02.deviantart.net/fs70/f/2015/018/4/a/chappie_fan_art_poster_by_gojirakaiju3d-d8eeo2m.jpg");
        tags = new ArrayList<Integer>();
        tags.add(Tags.CRIME);
        tags.add(Tags.ACTION);
        mSliderItem.setTagsList(tags);

        list.add(mSliderItem);



        mSliderItem = new SliderItem();
        mSliderItem.setTitle("Tears of the sun");
        mSliderItem.setThumbUrl("https://upload.wikimedia.org/wikipedia/en/thumb/d/d9/Tears_of_the_Sun_movie.jpg/230px-Tears_of_the_Sun_movie.jpg");
        tags = new ArrayList<Integer>();
        tags.add(Tags.CRIME);
        tags.add(Tags.ADVENTURE);
        tags.add(Tags.PREMIUM);
        mSliderItem.setTagsList(tags);

        list.add(mSliderItem);



        mSliderItem = new SliderItem();
        mSliderItem.setTitle("Star Trek Into Darkness");
        mSliderItem.setThumbUrl("http://cdn.bleedingcool.net/wp-content/uploads/2012/12/star-trek-into-darkness.jpg");
        tags = new ArrayList<Integer>();
        tags.add(Tags.FANTASY);
        tags.add(Tags.COMEDY);
        tags.add(Tags.SPECIAL);
        mSliderItem.setTagsList(tags);

        list.add(mSliderItem);

        //simulate network delay
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/

        try {
            MobileServiceClient client = new MobileServiceClient(Constant.AZURE_APPURL,
                    Constant.AZURE_APPKEY, Application.getInstance().getContext());
            MobileServiceTable<SliderItem> table = client.getTable("Movie", SliderItem.class);

            MobileServiceList<SliderItem> mobileServiceList = table.select("id", "title", "thumbUrl", "tagsJson").execute().get();

            for(SliderItem item: mobileServiceList){
                ArrayList<Integer> tagsList = new ArrayList<>();
                tagsList.add(Tags.LATEST);
                tagsList.add(Tags.FREE);
                item.setTagsList(tagsList);

                Log.d(Constant.DEBUG_TAG, item.getTitle());

                list.add(item);

            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return list;
    }

    @Override
    protected void onPostExecute(List<SliderItem> sliderItemList) {
        dataSource.onAllSliderItemsLoaded(sliderItemList);
        caller.onSlideItemsDataAvailable(sliderItemList);
    }
}
