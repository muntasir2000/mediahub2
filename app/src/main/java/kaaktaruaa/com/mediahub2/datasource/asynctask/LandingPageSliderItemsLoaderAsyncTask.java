package kaaktaruaa.com.mediahub2.datasource.asynctask;

import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

import kaaktaruaa.com.mediahub2.datasource.bean.SliderItem;
import kaaktaruaa.com.mediahub2.datasource.listener.OnSliderItemsLoadedListener;
import kaaktaruaa.com.mediahub2.datasource.listener.OnSlideItemsAvailableListener;
import kaaktaruaa.com.mediahub2.util.Tags;

/**
 * Created by muntasir on 6/17/15.
 */
public class LandingPageSliderItemsLoaderAsyncTask extends AsyncTask<Void, Void, List<SliderItem>>{

    OnSliderItemsLoadedListener cacheDestination;
    OnSlideItemsAvailableListener callerFragment;
    public LandingPageSliderItemsLoaderAsyncTask(OnSliderItemsLoadedListener cacheDestination, OnSlideItemsAvailableListener callerFragment){
        this.cacheDestination = cacheDestination;
        this.callerFragment = callerFragment;
    }

    @Override
    protected List<SliderItem> doInBackground(Void... voids) {
        ArrayList<SliderItem> list = new ArrayList<>();

        SliderItem mSliderItem = new SliderItem();
        mSliderItem.setTitle("Project Almanac");
        mSliderItem.setThumbUrl("http://ia.media-imdb.com/images/M/MV5BMTUxMjQ2NjI4OV5BMl5BanBnXkFtZTgwODc2NjUwNDE@._V1_SX214_AL_.jpg");
        List<Integer> tags = new ArrayList<Integer>();
        tags.add(Tags.POPULAR);
        mSliderItem.setTagsList(tags);

        list.add(mSliderItem);


        mSliderItem = new SliderItem();
        mSliderItem.setTitle("Avengers");
        mSliderItem.setThumbUrl("http://www.iceposter.com/thumbs/MOV_575e1c88_b.jpg");
        tags = new ArrayList<Integer>();
        tags.add(Tags.POPULAR);
        tags.add(Tags.NEW);
        mSliderItem.setTagsList(tags);

        list.add(mSliderItem);

        mSliderItem = new SliderItem();
        mSliderItem.setTitle("Chappie");
        mSliderItem.setThumbUrl("http://fc02.deviantart.net/fs70/f/2015/018/4/a/chappie_fan_art_poster_by_gojirakaiju3d-d8eeo2m.jpg");
        tags = new ArrayList<Integer>();
        tags.add(Tags.POPULAR);
        tags.add(Tags.NEW);
        mSliderItem.setTagsList(tags);

        list.add(mSliderItem);



        mSliderItem = new SliderItem();
        mSliderItem.setTitle("Tears of the sun");
        mSliderItem.setThumbUrl("https://upload.wikimedia.org/wikipedia/en/thumb/d/d9/Tears_of_the_Sun_movie.jpg/230px-Tears_of_the_Sun_movie.jpg");
        tags = new ArrayList<Integer>();
        tags.add(Tags.POPULAR);
        tags.add(Tags.NEW);
        mSliderItem.setTagsList(tags);

        list.add(mSliderItem);



        mSliderItem = new SliderItem();
        mSliderItem.setTitle("Star Trek Into Darkness");
        mSliderItem.setThumbUrl("http://cdn.bleedingcool.net/wp-content/uploads/2012/12/star-trek-into-darkness.jpg");
        tags = new ArrayList<Integer>();
        tags.add(Tags.POPULAR);

        mSliderItem.setTagsList(tags);

        list.add(mSliderItem);



        mSliderItem = new SliderItem();
        mSliderItem.setTitle("Fast & Furious 7");
        mSliderItem.setThumbUrl("https://upload.wikimedia.org/wikipedia/en/8/8f/Fast_and_Furious_Poster.jpg");
        tags = new ArrayList<Integer>();
        tags.add(Tags.POPULAR);
        tags.add(Tags.NEW);
        mSliderItem.setTagsList(tags);

        list.add(mSliderItem);

        //simulate network delay
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return list;
    }

    @Override
    protected void onPostExecute(List<SliderItem> sliderItems) {
        cacheDestination.onAllSliderItemsLoaded(sliderItems);
        callerFragment.onSlideItemsDataAvailable(sliderItems);
    }
}
