package kaaktaruaa.com.mediahub2.datasource.bean;

/**
 * Created by muntasir on 7/1/15.
 */
public class Movie {
    private String id;
    private String title;
    private String description;
    private String streamUrl;
    private String starringActorNamesListJson;
    private int releaseYear;
    private float rating;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStreamUrl() {
        return streamUrl;
    }

    public void setStreamUrl(String streamUrl) {
        this.streamUrl = streamUrl;
    }

    public String getStarringActorNamesListJson() {
        return starringActorNamesListJson;
    }

    public void setStarringActorNamesListJson(String starringActorNamesListJson) {
        this.starringActorNamesListJson = starringActorNamesListJson;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getGenreIdListJson() {
        return genreIdListJson;
    }

    public void setGenreIdListJson(String genreIdListJson) {
        this.genreIdListJson = genreIdListJson;
    }

    private String genreIdListJson;
}
