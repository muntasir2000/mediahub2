package kaaktaruaa.com.mediahub2.util;

import android.content.Context;

/**
 * Created by muntasir on 7/6/15.
 */
public class Application {
    private static Application ourInstance = new Application();

    private Context ourContext;
    public static Application getInstance() {
        return ourInstance;
    }

    private Application() {
    }

    public void setContext(Context context){
        this.ourContext = context;
    }

    public Context getContext(){
        return ourContext;
    }
}
