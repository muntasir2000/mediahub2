package kaaktaruaa.com.mediahub2.util;

/**
 * Created by muntasir on 6/7/15.
 */
public class Tags {
    public static int LATEST = 0;
    public static int FREE = 1;
    public static int PREMIUM = 2;
    public static int SPECIAL = 3;
    public static int ACTION = 4;
    public static int ADVENTURE = 5;
    public static int CLASSIC = 6;
    public static int COMEDY = 7;
    public static int CRIME = 8;
    public static int DRAMA = 9;
    public static int FANTASY = 10;
    public static int POPULAR = 11;
    public static int NEW = 12;
    public static int GENERAL = 13;
    public static int ENTERTAINMENT = 14;
    public static int NEWS = 15;
    public static int TYPE_MOVIE = 16;
    public static int TYPE_TV = 17;
    public static int TYPE_SHORTFILM = 18;
    public static int TYPE_DRAMASERIAL = 19;

}
